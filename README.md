# Dyff GCP project deployment

## Contents

Ordered by bootstrap sequence.

### GCP resources

- [dyff-cloud](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-cloud): GKE Autopilot cluster deployment.

- [dyff-storage](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-storage): GCS bucket deployment.

### On-cluster backing services

- [dyff-cert-manager](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-cert-manager): `cert-manager` deployment.

- [dyff-external-dns](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-external-dns): `external-dns` deployment.

- [dyff-ingress-nginx](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-ingress-nginx): `ingress-nginx` deployment.

- [dyff-kafka](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-kafka): Kafka deployment.

- [dyff-mongodb](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-mongodb): MongoDB deployment.

### Dyff services

- [dyff-api](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-api): Dyff API server deployment.

- [dyff-orchestrator](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-orchestrator): Dyff orchestrator deployment.

- [dyff-operator](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-operator): Dyff cluster operator deployment.

- [dyff-workflows-aggregator](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-workflows-aggregator): `workflows-aggregator` deployment.

- [dyff-workflows-informer](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-workflows-informer): `workflows-informer` deployment.

- [dyff-workflows-sink](https://gitlab.com/dsri-org/infra/gcp/dyff/dyff-workflows-sink): `workflows-sink` deployment.
